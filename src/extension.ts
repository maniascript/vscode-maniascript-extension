import * as vscode from 'vscode'
import * as completion from './completion.ts'

export async function activate(context: vscode.ExtensionContext) {
  await completion.updateApi()
	context.subscriptions.push(...completion.register())
}
