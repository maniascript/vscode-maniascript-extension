import * as vscode from 'vscode'
import { CodeCompletionCore } from 'antlr4-c3'
import { CharStream, CommonTokenStream, Token } from 'antlr4ng'
import { ManiaScriptLexer, ManiaScriptParser, parse, symbolTable } from '@maniascript/parser'
import * as maniascriptApi from '@maniascript/api'

const IGNORED_TOKENS = new Set([
  ManiaScriptLexer.CLASS,
  ManiaScriptLexer.SINGLE_LINE_COMMENT,
  ManiaScriptLexer.MULTI_LINES_COMMENT,
  ManiaScriptLexer.OPERATOR_ASSIGN,
  ManiaScriptLexer.OPERATOR_SEMICOLON,
  ManiaScriptLexer.OPERATOR_PLUS,
  ManiaScriptLexer.OPERATOR_MINUS,
  ManiaScriptLexer.OPERATOR_MULTIPLY,
  ManiaScriptLexer.OPERATOR_DIVIDE,
  ManiaScriptLexer.OPERATOR_MODULO,
  ManiaScriptLexer.OPERATOR_OPEN_TEXT_TRIPLE,
  ManiaScriptLexer.OPERATOR_CLOSE_INTERPOLATION,
  ManiaScriptLexer.OPERATOR_OPEN_INTERPOLATION_ERROR,
  ManiaScriptLexer.OPERATOR_CLOSE_INTERPOLATION_ERROR,
  ManiaScriptLexer.OPERATOR_OPEN_TEXT_SINGLE,
  ManiaScriptLexer.OPERATOR_OPEN_PAREN,
  ManiaScriptLexer.OPERATOR_CLOSE_PAREN,
  ManiaScriptLexer.OPERATOR_OPEN_BRACKET,
  ManiaScriptLexer.OPERATOR_CLOSE_BRACKET,
  ManiaScriptLexer.OPERATOR_OPEN_BRACE,
  ManiaScriptLexer.OPERATOR_CLOSE_BRACE,
  ManiaScriptLexer.OPERATOR_CONCAT,
  ManiaScriptLexer.OPERATOR_COMMA,
  ManiaScriptLexer.OPERATOR_DOUBLECOLON,
  ManiaScriptLexer.OPERATOR_COLON,
  ManiaScriptLexer.OPERATOR_DOT,
  ManiaScriptLexer.OPERATOR_NOT,
  ManiaScriptLexer.OPERATOR_LESSTHAN,
  ManiaScriptLexer.OPERATOR_MORETHAN,
  ManiaScriptLexer.OPERATOR_LESSTHANEQUAL,
  ManiaScriptLexer.OPERATOR_MORETHANEQUAL,
  ManiaScriptLexer.OPERATOR_EQUAL,
  ManiaScriptLexer.OPERATOR_NOTEQUAL,
  ManiaScriptLexer.OPERATOR_AND,
  ManiaScriptLexer.OPERATOR_OR,
  ManiaScriptLexer.OPERATOR_MULTIPLYASSIGN,
  ManiaScriptLexer.OPERATOR_DIVIDEASSIGN,
  ManiaScriptLexer.OPERATOR_PLUSASSIGN,
  ManiaScriptLexer.OPERATOR_MINUSASSIGN,
  ManiaScriptLexer.OPERATOR_MODULOASSIGN,
  ManiaScriptLexer.OPERATOR_ARROWASSIGN,
  ManiaScriptLexer.OPERATOR_CONCATASSIGN,
  ManiaScriptLexer.OPERATOR_TEXTTRANSLATE,
  ManiaScriptLexer.OPERATOR_ARROW,
  ManiaScriptLexer.LITERAL_REAL,
  ManiaScriptLexer.LITERAL_INTEGER,
  ManiaScriptLexer.IDENTIFIER,
  ManiaScriptLexer.WHITESPACES,
  ManiaScriptLexer.LINE_TERMINATOR,
  ManiaScriptLexer.UNKNOWN,
  ManiaScriptLexer.OPERATOR_CLOSE_TEXT_TRIPLE,
  ManiaScriptLexer.OPERATOR_OPEN_INTERPOLATION,
  ManiaScriptLexer.PART_TEXT_TRIPLE,
  ManiaScriptLexer.PART_TEXT_TRIPLE_QUOTE,
  ManiaScriptLexer.PART_TEXT_TRIPLE_BRACE,
  ManiaScriptLexer.OPERATOR_CLOSE_TEXT_SINGLE,
  ManiaScriptLexer.PART_TEXT_SINGLE,
  ManiaScriptLexer.EOF
])

const PREFERRED_RULES = new Set([
  ManiaScriptParser.RULE_className,
  ManiaScriptParser.RULE_extendsPath,
  ManiaScriptParser.RULE_includePath,
  ManiaScriptParser.RULE_namespaceDefinition,
  ManiaScriptParser.RULE_settingName,
  ManiaScriptParser.RULE_commandName,
  ManiaScriptParser.RULE_namespaceName,
  ManiaScriptParser.RULE_structName,
  ManiaScriptParser.RULE_constName,
  ManiaScriptParser.RULE_variableName,
  ManiaScriptParser.RULE_functionName,
  ManiaScriptParser.RULE_labelName,
  ManiaScriptParser.RULE_objectMemberName,
  ManiaScriptParser.RULE_structMemberName,
  ManiaScriptParser.RULE_enumName,
  ManiaScriptParser.RULE_enumValue
])

const TRIGGER_CHARACTERS = ['#', '.', ':', '"']

/*
const ALL_SCRIPT_CONTEXTS = [
  'CAction',
  'CAnyEditorPlugin',
  'CAnyEditorPluginLayer',
  'CEditorEditor',
  'CEditorMainPlugin',
  'CEditorMediaTracker',
  'CEditorMesh',
  'CEditorModule',
  'CEditorSkin',
  'CGameModuleEditorBase',
  'CGameScriptHandlerMediaTrack',
  'CManiaApp',
  'CManiaAppBase',
  'CManiaAppBrowser',
  'CManiaAppPlayground',
  'CManiaAppPlaygroundCommon',
  'CManiaAppStation',
  'CManiaAppTitle',
  'CManiaAppTitleLayer',
  'CManiaplanetPlugin',
  'CMapEditorPlugin',
  'CMapEditorPluginLayer',
  'CMapType',
  'CModuleMenu',
  'CModuleMenuLayer',
  'CMlBrowser',
  'CMlScript',
  'CMlScript_ReadOnly ',
  'CMlScriptIngame',
  'CMlScriptIngame_ReadOnly',
  'CMlStation',
  'CMode',
  'CServerPlugin',
  'CSmAction',
  'CSmArenaInterfaceManialinkScriptHandler_ReadOnly',
  'CSmMapType',
  'CSmMlScriptIngame',
  'CSmMode'
]
*/
const COMMON_SCRIPT_CONTEXTS = [
  'CManiaAppPlayground',
  'CManiaAppTitle',
  'CManiaAppTitleLayer',
  'CManiaplanetPlugin',
  'CMapEditorPlugin',
  'CMapEditorPluginLayer',
  'CMlScript',
  'CServerPlugin',
  'CSmAction',
  'CSmMapType',
  'CSmMlScriptIngame',
  'CSmMode'
]

let api = maniascriptApi.api

function moveToNextToken(token: Token): boolean {
  // If the token is not an identifier that can be completed
  return token.text !== undefined && /\W$/.test(token.text)
}

function findCaretTokenIndex (tokenStream: CommonTokenStream, position: vscode.Position): number | undefined {
  for (let i = 0; i < tokenStream.size; i++) {
    const token = tokenStream.get(i)
    const tokenStartLine = token.line - 1
    const tokenStartColumn = token.column
    const tokenEndColumn = token.column + (token.text?.length ?? 0)

    if (tokenStartLine === position.line && tokenStartColumn <= position.character && tokenEndColumn >= position.character) {
      if (moveToNextToken(token)) {
        if (i + 1 <= tokenStream.size) {
          return i + 1
        }
      }
      return i
    }
  }

  return undefined
}

function tokenTypeToDisplayName (lexer: ManiaScriptLexer, tokenType: number): string | null {
  let displayName = lexer.vocabulary.getDisplayName(tokenType)

  if (displayName !== null) {
    // Names are wrapped in '' in the lexer, remove them. eg: `'declare'` -> `declare`
    displayName = displayName.replace(/^'|'$/g, '')
  }

  return displayName
}

interface CompletionItemOptions {
  detail?: string,
  documentation?: string | vscode.MarkdownString
  insertText?: string | vscode.SnippetString
  range?: vscode.Range | { inserting: vscode.Range; replacing: vscode.Range }
}

class CompletionItemsCollector {
  #completionItems: vscode.CompletionItem[] = []

  push (kind: vscode.CompletionItemKind, symbolName: string, options?: CompletionItemOptions) {
    const completionItem = new vscode.CompletionItem(symbolName, kind)
    if (options?.detail !== undefined) {
      completionItem.detail = options.detail
    }
    if (options?.documentation !== undefined) {
      completionItem.documentation = options.documentation
    }
    if (options?.insertText !== undefined) {
      completionItem.insertText = options.insertText
    }
    if (options?.range !== undefined) {
      completionItem.range = options.range
    }
    this.#completionItems.push(completionItem)
  }

  getCompletionItems (): vscode.CompletionItem[] {
    return this.#completionItems
  }
}

class ManiascriptCompletionItemProvider implements vscode.CompletionItemProvider {
  public async provideCompletionItems(document: vscode.TextDocument, position: vscode.Position): Promise<vscode.CompletionItem[] | null> {
    if (!vscode.workspace.getConfiguration('mslang').get<boolean>('completionEnabled', true)) {
      return null
    }

    const completionItemsCollector = new CompletionItemsCollector()
    const completionItems: vscode.CompletionItem[] = []

    const inputStream = CharStream.fromString(document.getText())
    const lexer = new ManiaScriptLexer(inputStream)
    lexer.setClasses(api.classNames)

    const tokenStream = new CommonTokenStream(lexer)

    const parser = new ManiaScriptParser(tokenStream)
    parser.removeErrorListeners()
    const ctx = parser.program()

    const tokenIndex = findCaretTokenIndex(tokenStream, position)

    if (tokenIndex === undefined) {
      console.error('Could not find caret position in the token stream')
    } else {
      try {
        const c3 = new CodeCompletionCore(parser)
        c3.ignoredTokens = IGNORED_TOKENS
        c3.preferredRules = PREFERRED_RULES

        const candidates = c3.collectCandidates(tokenIndex, ctx.programContent())

        for (const candidate of candidates.tokens) {
          const keyword = tokenTypeToDisplayName(lexer, candidate[0])
          if (keyword !== null) {
            // Token `LITERAL_BOOLEAN` need to be converted to `True` and `False`
            if (keyword === 'LITERAL_BOOLEAN') {
              completionItemsCollector.push(vscode.CompletionItemKind.Keyword, 'True')
              completionItemsCollector.push(vscode.CompletionItemKind.Keyword, 'False')
            } else {
              completionItemsCollector.push(vscode.CompletionItemKind.Keyword, keyword)
            }
          }
        }

        const findSymbols = new Set<typeof symbolTable.BaseSymbol>()
        let findScriptPath = false
        let findSystemLibrary = false
        for (const rule of candidates.rules) {
          switch (rule[0]) {
            case ManiaScriptParser.RULE_className: {
              for (const className of lexer.classes) {
                completionItemsCollector.push(vscode.CompletionItemKind.Class, className)
              }
              break
            }
            case ManiaScriptParser.RULE_extendsPath: {
              findScriptPath = true
              break
            }
            case ManiaScriptParser.RULE_includePath: {
              findScriptPath = true
              findSystemLibrary = true
              break
            }
            case ManiaScriptParser.RULE_namespaceDefinition: {
              try {
                tokenStream.seek(tokenIndex)
                const path = tokenStream.LB(3)?.text ?? ''
                switch (path) {
                  case 'MathLib': {
                    completionItemsCollector.push(vscode.CompletionItemKind.Module, 'ML')
                    break
                  }
                  case 'TextLib': {
                    completionItemsCollector.push(vscode.CompletionItemKind.Module, 'TL')
                    break
                  }
                  case 'MapUnits': {
                    completionItemsCollector.push(vscode.CompletionItemKind.Module, 'MU')
                    break
                  }
                  case 'AnimLib': {
                    completionItemsCollector.push(vscode.CompletionItemKind.Module, 'AL')
                    break
                  }
                  case 'TimeLib': {
                    completionItemsCollector.push(vscode.CompletionItemKind.Module, 'TiL')
                    break
                  }
                  case 'ColorLib': {
                    completionItemsCollector.push(vscode.CompletionItemKind.Module, 'CL')
                    break
                  }
                  default: {
                    const prefix = path.match(/\/(?<prefix>Component|Mixin|UIModule)s\//i)?.groups?.['prefix'] ?? ''
                    const script = path.match(/(?<script>\w+)(?:@\d+)?\.Script\.txt$/i)?.groups?.['script'] ?? ''
                    if (script !== '') {
                      if (prefix !== '') {
                        completionItemsCollector.push(vscode.CompletionItemKind.Module, `${prefix}_${script}`)
                      } else {
                        completionItemsCollector.push(vscode.CompletionItemKind.Module, script)
                      }
                    }
                  }
                }
              } catch (error) {
                console.error(error)
              }
              break
            }
            case ManiaScriptParser.RULE_settingName: {
              // Todo
              break
            }
            case ManiaScriptParser.RULE_commandName: {
              // Todo
              break
            }
            case ManiaScriptParser.RULE_namespaceName: {
              findSymbols.add(symbolTable.NamespaceSymbol)
              break
            }
            case ManiaScriptParser.RULE_structName: {
              findSymbols.add(symbolTable.StructureSymbol)
              break
            }
            case ManiaScriptParser.RULE_constName: {
              findSymbols.add(symbolTable.ConstantSymbol)
              break
            }
            case ManiaScriptParser.RULE_variableName: {
              findSymbols.add(symbolTable.ConstantSymbol)
              findSymbols.add(symbolTable.VariableSymbol)
              findSymbols.add(symbolTable.SettingSymbol)
              findSymbols.add(symbolTable.CommandSymbol)
              findSymbols.add(symbolTable.ParameterSymbol)
              break
            }
            case ManiaScriptParser.RULE_functionName: {
              findSymbols.add(symbolTable.FunctionSymbol)
              break
            }
            case ManiaScriptParser.RULE_labelName: {
              findSymbols.add(symbolTable.LabelSymbol)
              break
            }
            case ManiaScriptParser.RULE_objectMemberName: {
              // Todo
              break
            }
            case ManiaScriptParser.RULE_structMemberName: {
              // Todo
              break
            }
            case ManiaScriptParser.RULE_enumName: {
              try {
                tokenStream.seek(tokenIndex)
                const tokenClass = tokenStream.LB(2)
                if (tokenClass?.text !== undefined && tokenClass.text in api.classes && api.classes[tokenClass.text].enums !== undefined) {
                  for (const enumName in api.classes[tokenClass.text].enums) {
                    completionItemsCollector.push(vscode.CompletionItemKind.Enum, enumName)
                  }
                }
              } catch (error) {
                console.error(error)
              }
              break
            }
            case ManiaScriptParser.RULE_enumValue: {
              try {
                tokenStream.seek(tokenIndex)
                const tokenClass = tokenStream.LB(4)
                const tokenEnumName = tokenStream.LB(2)
                if (tokenClass?.text !== undefined && tokenEnumName?.text !== undefined && tokenClass.text in api.classes) {
                  const enums = api.classes[tokenClass.text].enums
                  if (enums !== undefined && tokenEnumName.text in enums) {
                    for (const enumMember of enums[tokenEnumName.text]) {
                      completionItemsCollector.push(vscode.CompletionItemKind.EnumMember, enumMember)
                    }
                  }
                }
              } catch (error) {
                console.error(error)
              }
              break
            }
          }
        }

        // Find script files that can be included or extended
        if (findScriptPath) {
          const files = await vscode.workspace.findFiles('Scripts/**/*.Script.txt')
          for (const file of files) {
            if (file.scheme === 'file') {
              completionItemsCollector.push(
                vscode.CompletionItemKind.File,
                vscode.workspace.asRelativePath(file.fsPath).replace(/^Scripts\//, '')
              )
            }
          }
        }

        // Add system libraries (MathLib, TextLib, ...)
        if (findSystemLibrary) {
          for (const namespaceName of api.namespaceNames) {
            completionItemsCollector.push(vscode.CompletionItemKind.Module, namespaceName)
          }
        }

        // Find symbols of active script contexts
        if (
          findSymbols.has(symbolTable.VariableSymbol) ||
          findSymbols.has(symbolTable.FunctionSymbol)
        ) {
          let scriptContexts = Array.from(COMMON_SCRIPT_CONTEXTS)
          const directives = ctx.programContent().directives()?.directive() ?? []
          for (const directive of directives) {
            const requireContextDirective = directive.requireContextDirective()
            if (requireContextDirective !== null) {
              scriptContexts = [requireContextDirective.className().CLASS().getText()]
            }
          }

          const usedScriptContexts = new Set<string>()

          while (scriptContexts.length > 0) {
            const scriptContext = scriptContexts.shift()
            if (scriptContext !== undefined && !usedScriptContexts.has(scriptContext)) {
              usedScriptContexts.add(scriptContext)

              if (scriptContext in api.classes) {
                const variables = api.classes[scriptContext].variables
                const functions = api.classes[scriptContext].functions
                const enums = api.classes[scriptContext].enums
                const parent = api.classes[scriptContext].parent

                if (findSymbols.has(symbolTable.VariableSymbol)) {
                  if (variables !== undefined) {
                    for (const variableName in variables) {
                      completionItemsCollector.push(vscode.CompletionItemKind.Variable, variableName)
                    }
                  }

                  if (enums !== undefined) {
                    for (const enumName in enums) {
                      completionItemsCollector.push(vscode.CompletionItemKind.Enum, enumName)
                    }
                  }
                }

                if (findSymbols.has(symbolTable.FunctionSymbol) && functions !== undefined) {
                  for (const functionName in functions) {
                    completionItemsCollector.push(vscode.CompletionItemKind.Function, functionName)
                  }
                }

                if (parent !== undefined && !usedScriptContexts.has(parent)) {
                  scriptContexts.push(parent)
                }
              }
            }
          }
        }

        if (findSymbols.size > 0) {
          const result = await parse(document.getText(), { twoStepsParsing: true, buildSymbolTable: true })
          const symbols = result.symbolTable?.getSymbolsAtPosition(position.line + 1, position.character) ?? []
          for (const symbol of symbols) {
            if (symbol instanceof symbolTable.ConstantSymbol && findSymbols.has(symbolTable.ConstantSymbol)) {
              completionItemsCollector.push(vscode.CompletionItemKind.Constant, symbol.name)
            } else if (symbol instanceof symbolTable.VariableSymbol && findSymbols.has(symbolTable.VariableSymbol)) {
              completionItemsCollector.push(vscode.CompletionItemKind.Variable, symbol.name)
            } else if (symbol instanceof symbolTable.ParameterSymbol && findSymbols.has(symbolTable.ParameterSymbol)) {
              completionItemsCollector.push(vscode.CompletionItemKind.Constant, symbol.name)
            } else if (symbol instanceof symbolTable.NamespaceSymbol && findSymbols.has(symbolTable.NamespaceSymbol)) {
              completionItemsCollector.push(vscode.CompletionItemKind.Module, symbol.name)
            } else if (symbol instanceof symbolTable.StructureSymbol && findSymbols.has(symbolTable.StructureSymbol)) {
              completionItemsCollector.push(vscode.CompletionItemKind.Struct, symbol.name)
            } else if (symbol instanceof symbolTable.FunctionSymbol && findSymbols.has(symbolTable.FunctionSymbol)) {
              completionItemsCollector.push(vscode.CompletionItemKind.Function, symbol.name)
            } else if (symbol instanceof symbolTable.LabelSymbol && findSymbols.has(symbolTable.LabelSymbol)) {
              completionItemsCollector.push(vscode.CompletionItemKind.Snippet, symbol.name)
            } else if (symbol instanceof symbolTable.SettingSymbol && findSymbols.has(symbolTable.SettingSymbol)) {
              completionItemsCollector.push(vscode.CompletionItemKind.Constant, symbol.name)
            } else if (symbol instanceof symbolTable.CommandSymbol && findSymbols.has(symbolTable.CommandSymbol)) {
              completionItemsCollector.push(vscode.CompletionItemKind.Constant, symbol.name)
            }
          }
        }
      } catch (error: unknown) {
        console.error(error)
      }
    }

    // Complete directive keywords
    if (document.lineAt(position.line).text.includes('#')) {
      const range = document.getWordRangeAtPosition(position, /#\w*/)
      if (range !== undefined) {
        const keywords = ['#RequireContext', '#Extends', '#Include', '#Setting', '#Command', '#Struct', '#Const']
        for (const keyword of keywords) {
          const completionItem = new vscode.CompletionItem(keyword, vscode.CompletionItemKind.Keyword)
          completionItem.range = range
          completionItems.push(completionItem)
        }
      }
    }

    completionItems.push(...completionItemsCollector.getCompletionItems())

    return completionItems
  }
}

async function updateApi(): Promise<void> {
  const apiFilePath = vscode.workspace.getConfiguration('mslang').get<string>('apiFilePath', '')
  if (apiFilePath === '') {
    api = maniascriptApi.api
  } else {
    try {
      api = await maniascriptApi.generateFromFile(apiFilePath)
    } catch (error) {
      if (error instanceof Error) {
        void vscode.window.showErrorMessage(error.message)
      } else {
        void vscode.window.showErrorMessage('Failed to read ManiaScript API file provided in the settings')
      }
      api = maniascriptApi.api
    }
  }
}

function registerConfigurationWatcher(): vscode.Disposable {
  return vscode.workspace.onDidChangeConfiguration(async change => {
    if (change.affectsConfiguration('mslang.apiFilePath')) {
      await updateApi()
    }
  })
}

function register(): vscode.Disposable[] {
  return [
    registerConfigurationWatcher(),
    vscode.languages.registerCompletionItemProvider('maniascript', new ManiascriptCompletionItemProvider(), ...TRIGGER_CHARACTERS)
  ]
}

export {
  register,
  updateApi
}
