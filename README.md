Visual Studio Code ManiaScript Language extension
=================================================

![Visual Studio Marketplace Version](https://img.shields.io/visual-studio-marketplace/v/aessi.vscode-maniascript-language?label=Version)
![Visual Studio Marketplace Downloads](https://img.shields.io/visual-studio-marketplace/d/aessi.vscode-maniascript-language?label=Downloads)
![Visual Studio Marketplace Installs](https://img.shields.io/visual-studio-marketplace/i/aessi.vscode-maniascript-language?label=Install)
![Visual Studio Marketplace Rating](https://img.shields.io/visual-studio-marketplace/stars/aessi.vscode-maniascript-language?label=Rating)

Provide language support for ManiaScript.

Features
--------

* Syntax highlighting
* Autocompletion

Known Issues
------------

You should avoid using other extensions that provide the same type of services ([ManiaScript](https://marketplace.visualstudio.com/items?itemName=mmcfarland.vscode-maniascript), [maniascript-support](https://marketplace.visualstudio.com/items?itemName=reaby.maniascript-support), ...), as this will cause conflicts between them.

Release Notes
-------------

Check [CHANGELOG.md](CHANGELOG.md).
