Changelog
=========

Unreleased
----------

### Added

- Nothing yet ⏳

2.3.0
-----

### Added

- [#27](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/27) Added autocompletion for `#Include` namespace

2.2.0
-----

### Added

- [#25](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/25) Added autocompletion for enum names and values

### Fixed

- [#24](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/24) Fixed `apiFilePath` setting initialization

2.1.0
-----

### Added

- [#22](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/22) Added variables and functions from script context to the autocompletion

### Changed

- [#23](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/23) Improved `PART_TEXT_TRIPLE` autocompletion

2.0.0
-----

### Added

- [#20](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/20) Added basic autocompletion

1.4.1
-----

### Fixed

- [#21](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/21) Fixed nested comments in function definitions

1.4.0
-----

### Added

- [#19](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/19) Added maniascript highlighting in markdown code block

### Changed

- [#18](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/18) Improved syntax highlighting

1.3.1
-----

### Fixed

- [#17](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/17) Fixed `@Debug` tag in multiline comment not stopping after `*/`

1.3.0
-----

### Added

- [#15](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/15) Added maniascript highlighting inside `<script>` nodes
- [#16](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/16) Added highlighting of `@Debug` tag in comments

### Changed

- [#14](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/14) Added more padding on the left of the maniascript file icon

1.2.0
-----

### Added

- [#11](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/11) Added an icon for maniascript files
- [#12](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/12) Added javadoc comments highlighting
- [#13](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/13) Added xml highlighting in triple quoted strings

### Changed

- [#9](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/9) Improved multiline rules highlighting

### Fixed

- [#10](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/10) Fixed structure expression with a namespace

1.1.0
-----

### Added

- [#7](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/7) Added highlighting of `Int2 `, `Int3`, `Vec2` and `Vec3` literals

### Changed

- [#6](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/6) Improved constant declaration highlighting
- [#8](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/8) Improved multiline directives highlighting

1.0.0
-----

### Added

- [#1](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/issues/1) Implemented syntax highlighting
- [#2](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/work_items/3) Added a readme file
- [#2](https://gitlab.com/maniascript/vscode-extension-maniascript-lang/-/work_items/5) Added an icon for the extension
