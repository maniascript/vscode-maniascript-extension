{
  "name": "vscode-maniascript-language",
  "displayName": "MSLang",
  "description": "Support for ManiaScript language (syntax highlighting, autocomplete, ...).",
  "version": "2.3.0",
  "author": "Aessi <code@aessi.dev>",
  "license": "LGPL-3.0-or-later",
  "homepage": "https://gitlab.com/maniascript/vscode-extension-maniascript-lang#readme",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/maniascript/vscode-extension-maniascript-lang.git"
  },
  "bugs": {
    "url": "https://gitlab.com/maniascript/vscode-extension-maniascript-lang/issues"
  },
  "publisher": "aessi",
  "categories": [
    "Programming Languages"
  ],
  "keywords": [
    "maniascript",
    "language",
    "vscode",
    "maniaplanet",
    "trackmania",
    "shootmania",
    "questmania"
  ],
  "icon": "images/mslang_icon.png",
  "galleryBanner": {
    "color": "#f08080",
    "theme": "light"
  },
  "pricing": "Free",
  "engines": {
    "vscode": "^1.91.0"
  },
  "type": "module",
  "main": "./dist/extension.cjs",
  "exports": {
    "require": {
      "default": "./dist/extension.cjs"
    }
  },
  "contributes": {
    "languages": [
      {
        "id": "maniascript",
        "aliases": [
          "ManiaScript",
          "Maniascript",
          "maniascript"
        ],
        "extensions": [
          ".Script.txt",
          ".script.txt"
        ],
        "configuration": "./language-configuration.json",
        "icon": {
          "dark": "./images/maniascript_icon_dark.svg",
          "light": "./images/maniascript_icon_light.svg"
        }
      }
    ],
    "grammars": [
      {
        "language": "maniascript",
        "scopeName": "source.maniascript",
        "embeddedLanguages": {
          "meta.tag.xml": "xml"
        },
        "path": "./syntaxes/maniascript.tmLanguage.json"
      },
      {
        "scopeName": "meta.embedded.maniascript.expression",
        "embeddedLanguages": {
          "meta.embedded.maniascript": "maniascript"
        },
        "injectTo": [
          "source.maniascript"
        ],
        "path": "./syntaxes/maniascript.xmlInjection.json"
      },
      {
        "scopeName": "markdown.maniascript.codeblock",
        "embeddedLanguages": {
          "meta.embedded.block.manianiascript": "maniascript"
        },
        "injectTo": [
          "text.html.markdown"
        ],
        "path": "./syntaxes/maniascript.markdownInjection.json"
      }
    ],
    "configuration": {
      "title": "MSLang",
      "properties": {
        "mslang.completionEnabled": {
          "type": "boolean",
          "default": true,
          "description": "Enable/disable autocomplete suggestions.",
          "scope": "window"
        },
        "mslang.apiFilePath": {
          "type": "string",
          "format": "path",
          "scope": "resource",
          "default": "",
          "description": "Path to a ManiaScript API file to be used instead of the one provided by the extension."
        }
      }
    }
  },
  "nodemonConfig": {
    "ext": ".tmLanguage.json,.test.maniascript,.snap.maniascript,.xmlInjection.json,.markdownInjection.json",
    "ignore": [
      ".vscode"
    ]
  },
  "scripts": {
    "vscode:prepublish": "npm run test:grammars && npm run build:production",
    "lint": "eslint src/**/*.ts",
    "typecheck": "tsc --project tsconfig.json",
    "cleanup": "rimraf ./dist",
    "build:dev": "npm run cleanup && npm run lint && npm run typecheck && node esbuild.config.js",
    "build:production": "npm run cleanup && npm run lint && npm run typecheck && node esbuild.config.js --production",
    "watch:esbuild": "node esbuild.config.js --watch",
    "watch:tsc": "tsc --project tsconfig.json --watch",
    "test:extension:compile": "rimraf ./out && tsc --project tsconfig.test.json",
    "test:extension:watch": "rimraf ./out && tsc --project tsconfig.test.json --watch",
    "test:extension": "npm run test:extension:compile && npm run build:dev && npm run lint && vscode-test",
    "dev:extension": "npm-run-all --parallel watch:*",
    "test:grammars:snapupdate": "vscode-tmgrammar-snap --updateSnapshot --scope \"source.maniascript\" \"tests/grammars/**/*.snap.maniascript\"",
    "test:grammars:snapcheck": "vscode-tmgrammar-snap --scope \"source.maniascript\" \"tests/grammars/**/*.snap.maniascript\"",
    "test:grammars:test": "vscode-tmgrammar-test \"tests/grammars/**/*.test.maniascript\"",
    "test:grammars": "npm run test:grammars:test && npm run test:grammars:snapcheck",
    "dev:grammars": "nodemon --exec \"npm run test:grammars\""
  },
  "devDependencies": {
    "@eslint/js": "^9.7.0",
    "@types/eslint__js": "^8.42.3",
    "@types/mocha": "^10.0.7",
    "@types/node": "^20.14.11",
    "@types/vscode": "^1.91.0",
    "@vscode/test-cli": "^0.0.10",
    "@vscode/test-electron": "^2.4.1",
    "esbuild": "^0.23.0",
    "eslint": "^8.57.0",
    "nodemon": "^3.1.4",
    "npm-run-all": "^4.1.5",
    "rimraf": "^6.0.1",
    "typescript": "^5.5.3",
    "typescript-eslint": "^7.16.1",
    "vscode-tmgrammar-test": "^0.1.2"
  },
  "dependencies": {
    "@maniascript/api": "^8.1.0",
    "@maniascript/parser": "^15.0.0",
    "antlr4-c3": "^3.4.1",
    "antlr4ng": "^3.0.4"
  }
}
